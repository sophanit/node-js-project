var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
var url = require('url');
var swagger = require("swagger-node-express");

const port = 3000;
const hostname = '127.0.0.1';

var drivers = require('./routes/driversRoute');
var customers = require('./routes/customerRoute');

var app = express();

// view engine setup
/*app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');*/

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

swagger.setAppHandler(app);

swagger.addValidator(
  function validate(req, path, httpMethod) {
    //  example, only allow POST for api_key="special-key" 
    if ("POST" == httpMethod || "DELETE" == httpMethod || "PUT" == httpMethod) {
      var apiKey = req.headers["api_key"];
      if (!apiKey) {
        apiKey = url.parse(req.url,true).query["api_key"];
      }
      if ("special-key" == apiKey) {
        return true; 
      }
      return false;
    }
    return true;
  }
);

app.use('/api/v1/', drivers);
app.use('/api/v1/', customers);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});

/*io.sockets.on('connection', function(socket){
  connections.push(socket);
  console.log('Connected: %s socket Connected', connections.length);

  connections.splice(connections.indexOf(socket), 1);
  console.log('Disconnected: %s Socket Connected', connections.length);

  socket.on('sending message', function(data){
    io.sockets.emit('new Message', {msg : data});
  });

  sockets.on('new users', function(data, callback){
    callback(true);
    socket.users
  });

});*/

module.exports = app;