var express   =    require("express");
var mysql     =    require('mysql');
var app       =    express();

var connection      =    mysql.createPool({
    connectionLimit : 100, //important
    host     : 'localhost',
    user     : 'root',
    port 	 : '3306',
    database : 'taxidatabase',
    debug    :  false
});

exports.connection = connection;
