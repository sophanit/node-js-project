var express = require('express');
var router = express.Router();
var conn = require('../helper/configuration');
var connection = conn.connection;
Customers = require('../models/customerModel');

function getAllCustomers(req,res) {
    
    connection.getConnection(function(err,connection){
        if (err) {
          res.json({"code" : 404, "status" : "Error in connection database"});
          return;
        }   

        console.log('connected as id ' + connection.threadId);
        
        connection.query("SELECT Table_customers.customer_id, Table_customers.customer_name, Table_customers.customer_location, Table_customers.customer_area FROM table_customers",function(err,rows){
            connection.release();
            if(!err) {
                res.json({data:rows});
            }           
        });

        connection.on('error', function(err) {      
              res.json({"code" : 404, "status" : "Error in connection database"});
              return;     
        });
  });
}

router.get('/customers', function(req, res) {
  getAllCustomers(req, res);
  /*Customers.getAllCustomers(function(err, customer){
    if (err) {
      throw err;
    }else{
      res.json({data : customer});
    }
  });*/
});

module.exports = router;