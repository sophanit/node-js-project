var express = require('express');
var router = express.Router();
var conn = require('../helper/configuration');
var connection = conn.connection;

function getAllDrivers(req,res) {
    
    connection.getConnection(function(err,connection){
        if (err) {
          res.json({"code" : 404, "status" : "Error in connection database"});
          return;
        }   

        console.log('connected as id ' + connection.threadId);
        
        connection.query("SELECT Table_drivers.driver_id, Table_drivers.driver_first_name, Table_drivers.driver_last_name, Table_drivers.driver_mobile_phone, Table_drivers.driver_address, Table_drivers.driver_area FROM table_drivers",function(err,rows){
            connection.release();
            if(!err) {
                res.json({data:rows});
            }           
        });

        connection.on('error', function(err) {      
              res.json({"code" : 404, "status" : "Error in connection database"});
              return;     
        });
  });
}

router.get('/drivers', function(req, res) {
  getAllDrivers(req,res);
});

module.exports = router;